const fs = require("fs");
const {searchFirstNumber, searchLastNumber} = require("../utils/search");

const LINE_BREAK = '\r\n';

function solver1(input) {
  let sum = 0;
  input.split(LINE_BREAK).forEach(line => {
    const numbers = [];
    line.split('').filter(c => {
      if(!isNaN(parseInt(c))) {
        numbers.push(parseInt(c));
      }
    })
    sum += parseInt(`${numbers[0]}${numbers[numbers.length - 1]}`)
  })
  return sum;
}

function solver2(input) {
  let sum = 0;
  input.split(LINE_BREAK).forEach(line => {
    const firstNumber = searchFirstNumber(line)
    const lastNumber = searchLastNumber(line)

    sum += parseInt(`${firstNumber}${lastNumber}`);
  })
  return sum;
}

const input = fs.readFileSync('input.txt', "utf-8");

console.time()

// part 1
console.log(solver1(input))

// part 2
console.log(solver2(input))

console.timeEnd()