const fs = require("fs");

const LINE_BREAK = '\r\n';

const gameConstraint = new Map([
  ['red', 12],
  ['green', 13],
  ['blue', 14]
])

function getMaxValues(gameDetail) {
  let maxValues = new Map();
  gameDetail.split('; ').forEach(set => {
    set.split(', ').forEach(cube => {
      const [number, color] = cube.split(' ');
      if (!maxValues.has(color)) {
        maxValues.set(color, parseInt(number));
        return;
      }
      if (maxValues.get(color) < parseInt(number)) {
        maxValues.set(color, parseInt(number));
      }
    })
  });
  return maxValues;
}

function solver1(input) {
  let sum= 0;
  input.split(LINE_BREAK).forEach(game => {
    const [gameId, gameDetail] = game.split(': ');
    let maxValues = getMaxValues(gameDetail);

    let isValid = true;
    maxValues.forEach((value, key) => {
      if (gameConstraint.get(key) < value) {
        isValid = false;
      }
    })
    if(isValid) {
      sum += parseInt(gameId.split(' ')[1]);
    }
  });
  return sum;
}

function solver2(input) {
  let sum= 0;
  input.split(LINE_BREAK).forEach(game => {
    const [_, gameDetail] = game.split(': ');
    let maxValues = getMaxValues(gameDetail);

    let power;
    maxValues.forEach((value) => {
      if(power === undefined) {
        power = value;
      } else {
        power *= value;
      }
    })
    if(power !== undefined) {
      sum += power;
    }
  });
  return sum;
}

const input = fs.readFileSync('input.txt', "utf-8");

console.time()
// part 1
console.log(solver1(input))

// part 2
console.log(solver2(input))

console.timeEnd()