const fs = require("fs");

const LINE_BREAK = '\r\n';

const NUMBER_REGEX = /\d+/gm
const SYMBOL_REGEX = /[^\d|.|\r\n]/gm
const ASTERISK_SYMBOL_REGEX = /\*/gm

const input = fs.readFileSync('input.txt', "utf-8");
function solver1(input) {
  const SCHEMATIC_LINE_LENGHT = input.indexOf("\r\n");
  const inlineInput = input.replaceAll("\r\n", "");
  let knownSymbolsPos = [];
  let sum = 0;
  let search;

  while((search = SYMBOL_REGEX.exec(inlineInput)) !== null) {
    SYMBOL_REGEX.lastIndex++;
    const charPos = search.index;
    const x = Math.floor((charPos) / SCHEMATIC_LINE_LENGHT);
    const y = charPos - (x * SCHEMATIC_LINE_LENGHT)
    knownSymbolsPos.push([x,y]);
  }

  while((search = NUMBER_REGEX.exec(inlineInput)) !== null) {
    NUMBER_REGEX.lastIndex++;

    const startChar = search.index;
    const startX = Math.floor((startChar) / SCHEMATIC_LINE_LENGHT);
    const startY = startChar - (startX * SCHEMATIC_LINE_LENGHT);
    const start = [Math.max(startX - 1, 0), Math.max(startY - 1, 0)];

    const endChar = startChar + search[0].length - 1;
    const endX = Math.floor((endChar) / SCHEMATIC_LINE_LENGHT);
    const endY = endChar - (endX * SCHEMATIC_LINE_LENGHT);
    const end = [Math.min(endX + 1, SCHEMATIC_LINE_LENGHT - 1), Math.min(endY + 1, SCHEMATIC_LINE_LENGHT - 1)];


    if (knownSymbolsPos.some(coord => {
      //console.log(`${search[0]} ${coord} -> ${[start, end]}: ${start[0] <= coord[0]} ${start[1] <= coord[1]} ${end[0] >= coord[0]} ${end[1] >= coord[1]}`)
      return (start[0] <= coord[0] && start[1] <= coord[1])
        && (end[0] >= coord[0] && end[1] >= coord[1]);
    })) {
      sum += parseInt(search[0]);
      continue;
    }
    //console.log("Discarded " + search[0])
  }
  return sum;
}

function solver2(input) {
  const SCHEMATIC_LINE_LENGHT = input.indexOf("\r\n");
  const inlineInput = input.replaceAll("\r\n", "");
  let knownSymbolsPos = [];
  let symbolsSum = new Map();
  let search;

  while((search = ASTERISK_SYMBOL_REGEX.exec(inlineInput)) !== null) {
    ASTERISK_SYMBOL_REGEX.lastIndex++;
    const charPos = search.index;
    const x = Math.floor((charPos) / SCHEMATIC_LINE_LENGHT);
    const y = charPos - (x * SCHEMATIC_LINE_LENGHT)
    knownSymbolsPos.push([x,y]);
  }

  while((search = NUMBER_REGEX.exec(inlineInput)) !== null) {
    NUMBER_REGEX.lastIndex++;

    const startChar = search.index;
    const startX = Math.floor((startChar) / SCHEMATIC_LINE_LENGHT);
    const startY = startChar - (startX   * SCHEMATIC_LINE_LENGHT);
    const start = [Math.max(startX - 1, 0), Math.max(startY - 1, 0)];

    const endChar = startChar + search[0].length - 1;
    const endX = Math.floor((endChar) / SCHEMATIC_LINE_LENGHT);
    const endY = endChar - (endX * SCHEMATIC_LINE_LENGHT);
    const end = [Math.min(endX + 1, SCHEMATIC_LINE_LENGHT - 1), Math.min(endY + 1, SCHEMATIC_LINE_LENGHT - 1)];


    const relatedSymbols = knownSymbolsPos.filter(coord => {
      //console.log(`${search[0]} ${coord} -> ${[start, end]}: ${start[0] <= coord[0]} ${start[1] <= coord[1]} ${end[0] >= coord[0]} ${end[1] >= coord[1]}`)
      return (start[0] <= coord[0] && start[1] <= coord[1])
        && (end[0] >= coord[0] && end[1] >= coord[1]);
    });

    relatedSymbols.forEach(symbolPos => {
      if(!symbolsSum.has(symbolPos)) {
        symbolsSum.set(symbolPos, []);
      }
      symbolsSum.get(symbolPos).push(search);
    })
    //console.log("Discarded " + search[0])
  }
  let sum = 0;
  symbolsSum.forEach((v) => {
    if(v.length === 2) {
      sum += parseInt(v[0][0]) * parseInt(v[1][0]);
    }
  })
  return sum;
}


console.time()
// part 1
console.log(solver1(input))

NUMBER_REGEX.lastIndex = 0;
// part 2
console.log(solver2(input));

console.timeEnd()

