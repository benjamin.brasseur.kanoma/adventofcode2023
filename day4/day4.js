const fs = require("fs");

const LINE_BREAK = '\r\n';

const input = fs.readFileSync('input.txt', "utf-8");
function solver1(input) {
  let sum = 0;
  input.split(LINE_BREAK)
    .map(line => line.split(': ')[1].split(' | '))
    .map(table => table.map(numbers => numbers.replaceAll('  ', ' ').trim().split(' ')))
    .forEach(value => {
      const nbGagnant = value[1].filter(n => value[0].some(w => w === n)).length - 1
      sum += Math.floor(Math.pow(2, nbGagnant));
    });
  return sum;
}

function solver2(input) {
  const regex = /\d+$/gm
  let storedCards = new Map()
  input.split(LINE_BREAK)
    .forEach(line => {
      const lineDetail = line.split(': ');
      const lineId = parseInt(lineDetail[0].match(regex)[0]);
      const value = lineDetail[1].split(' | ')
        .map(numbers => numbers.replaceAll('  ', ' ').trim().split(' '))

      const nbGagnant = value[1].filter(n => value[0].some(w => w === n)).length
      if(nbGagnant < 0) {
        return;
      }
      for (let i = 0; i <=  nbGagnant; i++) {
        const id = lineId + i;
        if(!storedCards.has(id)) {
          storedCards.set(id, [0, 0])
        }
        if(id !== lineId) {
          storedCards.get(id)[1] += storedCards.get(lineId)[1];
          continue;
        }
        storedCards.get(id)[1]++;
      }
      storedCards.get(lineId)[0] = nbGagnant;
    })

  return Array.from(storedCards.values()).map(c => c[1]).reduce((prev, cur) => prev + cur);
}


console.time()
// part 1
console.log(solver1(input))

// part 2
console.log(solver2(input));

console.timeEnd()
