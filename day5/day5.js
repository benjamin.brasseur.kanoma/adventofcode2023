const fs = require("fs");

const input = fs.readFileSync('input.txt', "utf-8");
function solver1(input) {
    const DATA_SEARCH = /seeds: (.+)|(.*) map:|(.+)/gm
    let convertingData = [];

    let currentConverter;
    let search;
    while((search = DATA_SEARCH.exec(input)) !== null) {
        DATA_SEARCH.lastIndex++;
        if(search[1]) {
            convertingData = search[1].split(' ').map(v => [parseInt(v), null]);
            continue;
        }
        if(search[2]) {
            currentConverter = search[2];
            continue;
        }
        if(search[3]) {
            const ranges = search[3].split(' ').map(v => parseInt(v));
            convertingData.forEach(v => {
                if(v[1] !== currentConverter && v[0] >= ranges[1] && v[0] < ranges[1] + ranges[2]) {
                    v[0] = ranges[0] + v[0] - ranges[1];
                    v[1] = currentConverter;
                }
            });
        }
    }
    return Math.min(...convertingData.map(v => v[0]));
}

function solver2(input) {
    const DATA_SEARCH = /seeds: (.+)|(.*) map:|(.+)/gm
    let convertingData = [];

    let currentConverter;
    let search;
    while((search = DATA_SEARCH.exec(input)) !== null) {
        DATA_SEARCH.lastIndex++;
        if(search[1]) {
            const numbers = search[1].split(' ');
            for(let i = 0; i < numbers.length; i+=2) {
                convertingData.push([[parseInt(numbers[i]), parseInt(numbers[i]) + parseInt(numbers[i + 1])], null])
            }
            continue;
        }
        if(search[2]) {
            currentConverter = search[2];
            continue;
        }
        if(search[3]) {
            const ranges = search[3].split(' ').map(v => parseInt(v));

            let i = 0;
            for(const v of convertingData) {
                i++;
                //console.log(v)
                if(v[1] !== currentConverter) {
                    const endRange = ranges[1] + ranges[2];
                    if(v[0][0] >= ranges[1] && v[0][0] < endRange) {
                        if(v[0][1] < endRange) {
                            v[0] = [ranges[0] + v[0][0] - ranges[1], ranges[0] + v[0][1] - ranges[1]];
                            v[1] = currentConverter;
                        } else {
                            let nextV = [[endRange, v[0][1]], null];
                            v[0] = [ranges[0] + v[0][0] - ranges[1], ranges[0] + ranges[2] - 1];
                            v[1] = currentConverter;
                            convertingData.push(nextV);
                        }
                    } else if(v[0][0] < ranges[1]) {
                        if(v[0][1] >= ranges[1] && v[0][1] < endRange) {
                            let prevV = [[v[0][0], ranges[1] - 1], null];
                            v[0] = [ranges[0], ranges[0] + v[0][1] - ranges[1]];
                            v[1] = currentConverter;
                            convertingData.push(prevV);
                        } else if(v[0][1] >= endRange) {
                            let prevV = [[v[0][0], ranges[1] - 1], null];
                            let nextV = [[endRange, v[0][1]], null];
                            v[0] = [ranges[0], ranges[0] + ranges[2] - 1];
                            v[1] = currentConverter;
                            convertingData.push(prevV);
                            convertingData.push(nextV);
                        }
                    }
                }
            }
        }
    }
    console.log(convertingData.length)
    return Math.min(...convertingData.map(v => v[0][0]));
}

console.time()
// part 1
console.log(solver1(input))

// part 2
console.log(solver2(input));

console.timeEnd()