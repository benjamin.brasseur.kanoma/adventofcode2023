const fs = require("fs");

const input = fs.readFileSync('input.txt', "utf-8");
const INPUTS_REGEX = /\d+/gm;
function solver1(input) {
    let data = input.match(INPUTS_REGEX);
    const lenght = data.length / 2;
    let sum = 1;
    for(let i = 0; i < lenght; i++) {
        const [time, distance] = [parseInt(data[i]), parseInt(data[i + lenght])];
        let holdMin = Math.ceil((time - Math.sqrt(Math.pow(time, 2) - 4 * distance)) / 2);
        let holdMax = Math.floor((time + Math.sqrt(Math.pow(time, 2) - 4 * distance)) / 2);
        if(Math.round((time - holdMin) * holdMin) <= distance) {
            holdMin++;
        }
        if(Math.round((time - holdMax) * holdMax) <= distance) {
            holdMax--;
        }
        sum *= holdMax - holdMin + 1;
    }
    return sum;
}

function solver2(input) {
    let data = input.match(INPUTS_REGEX);
    let lenght = data.length / 2;
    let time = parseInt(data.splice(0, lenght).join(''));
    let distance = parseInt(data.splice(0, lenght).join(''));

    let holdMin = Math.ceil((time - Math.sqrt(Math.pow(time, 2) - 4 * distance)) / 2);
    let holdMax = Math.floor((time + Math.sqrt(Math.pow(time, 2) - 4 * distance)) / 2);
    if(Math.round((time - holdMin) * holdMin) <= distance) {
        holdMin++;
    }
    if(Math.round((time - holdMax) * holdMax) <= distance) {
        holdMax--;
    }
    return holdMax - holdMin + 1;
}

console.time()
// part 1
console.log(solver1(input));

// part 2
console.log(solver2(input));

console.timeEnd()