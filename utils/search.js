const bindings = new Map([
  ['one', '1'],
  ['two', '2'],
  ['three', '3'],
  ['four', '4'],
  ['five', '5'],
  ['six', '6'],
  ['seven', '7'],
  ['eight', '8'],
  ['nine', '9'],
])

const regex = new RegExp(`(?=(${['\\d',...bindings.keys()]
  .map(b => `${b}`)
  .join('|')}))`, 'gm');

module.exports = {
  searchFirstNumber: function (textNumber = "") {
    let searchResult = regex.exec(textNumber)[1];
    return bindings.has(searchResult) ? bindings.get(searchResult) : searchResult;

  },
  searchLastNumber: function (textNumber) {
    let search;
    let searchResult = '0';
    while ((search = regex.exec(textNumber)) !== null) {
      regex.lastIndex++
      searchResult = search[1];
    }

    return bindings.has(searchResult) ? bindings.get(searchResult) : searchResult;
  }
}